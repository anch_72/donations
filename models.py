import sqlalchemy, os
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy import create_engine, Boolean, Column, ForeignKey, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.selectable import FromClause
#from sqlalchemy.dialects.postgresql import JSONB
import pendulum

file_path = os.path.join(os.getcwd(),"/donations.db")
engine = create_engine(f"sqlite:///{file_path}", echo=True)
Session = sessionmaker(bind=engine) 
Base = declarative_base()

#User-DonationConfig: one to many 
#DonationConfig-DonationRequest: one to one
class User(Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    mobile = Column(String, unique=True)
    email = Column(String)
    city = Column(String)
    pincode = Column(String)
    pancard = Column(String)
    address = Column(String)
    donation_configs = relationship("DonationConfig", back_populates='user')

class DonationConfig(Base):
    
    #When submitting a donation an account creates a donation config.
    
    __tablename__ = "donationconfig"
    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("user.id"))
    amount = Column(Integer, nullable=False)
    freq = Column(Integer, nullable=False)
    order_id = Column(String)
    cust_id = Column(String)
    rzp_payment_id = Column(String)
    rzp_payment_sig = Column(String)
    rzp_order_id = Column(String)
    user = relationship("User", back_populates='donation_configs')
    donation_request = relationship("Donation", back_populates="donation_configs")  
    
class Donation(Base):
    __tablename__ = "donation"
    id = Column(Integer, primary_key=True, index=True)
    donation_config_id = Column(Integer, ForeignKey('donationconfig.id'))
    created = Column(DateTime, default=lambda: pendulum.now("Asia/Kolkata"))
    donation_configs = relationship("DonationConfig", back_populates="donation_request")

Base.metadata.create_all(engine)
