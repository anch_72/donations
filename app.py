from re import template
from flask import Flask, render_template,redirect,url_for, request
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.functions import user
import razorpay, json, requests
from requests.models import Response
import models


app = Flask(__name__)

@app.route('/')
def home():
    return "Welcome home" 

@app.route('/donation/request/8/callback', methods=["POST", "GET"])
def payment():
    db = models.Session()
    return True

@app.route('/donation/request/8/config', methods=["POST", "GET"])
def donation_request():
    """
    Request to parse form data into Razorpay API's
    """

    email: str =request.form.get('email')
    mobile: str =request.form.get('mobile')
    name: str =request.form.get('name')
    pan: str =request.form.get('pan')
    city: str =request.form.get('city')
    addr: str =request.form.get('addr')
    pincode: str =request.form.get('pincode')
    freq: int =request.form.get('freq')
    amount: int =request.form.get('amount')

    db = models.Session()

    #check if the user already exists and create one only if it does not exists
    query = db.query(models.User.id).filter(models.User.email==email).first()
    if not query:
        new_user = models.User(name=name, mobile=mobile, email=email, city=city, pincode=pincode, pancard=pan, address=addr)
        db.add(new_user)
        db.commit()
    u_id = db.query(models.User.id).filter(models.User.email==email).first()
    print(u_id)
    new_donation_req = models.DonationConfig(user_id=u_id[0], amount=amount, freq=freq)
    db.add(new_donation_req)
    db.commit()

    #Creating customer ID
    client = razorpay.Client(auth=("rzp_test_JH8NWoQAIddV8m", "3EprLgHNQ2kYBeKCwx4kgNaH"))
    cust_url = "https://api.razorpay.com/v1/customers"
    cust_data = {
        "name": name,
        "email": email,
        "contact": mobile,
        "fail_existing": "0"}

    cust_id = client.customer.create(data=cust_data)
    print(f"cust id - {cust_id['id']}")
    url="https://api.razorpay.com/v1/orders"
    order_data = {
        "amount": 0,
        "currency": "INR",
        "payment_capture": "1",
        "method": "emandate",
        "customer_id": cust_id['id'],
        "receipt": "Receipt No. 1",
        "notes": {
            "notes_key_1": "Beam me up Scotty",
            "notes_key_2": "Engage"
        },
        "token": {
            "auth_type": "aadhaar",
            "max_amount": 9999900,
            "expire_at": 4102444799,
            "bank_account": {
            "beneficiary_name": "Gaurav Kumar",
            "account_number": "1121431121541121",
            "account_type": "savings",
            "ifsc_code": "HDFC0000001"
            },
            "notes": {
            "notes_key_1": "Tea, Earl Grey, Hot",
            "notes_key_2": "Tea, Earl Grey… decaf."
            }
        }
    }
    resp = requests.post(url , json=order_data, auth=('rzp_test_JH8NWoQAIddV8m', '3EprLgHNQ2kYBeKCwx4kgNaH'))
    oid = resp.json()['id']
    print(resp.status_code, resp.text)
    
    return render_template('rzp_auth.html', title="Donation",
                            frequency=freq, request=request, customer_id=cust_id['id'],
                            rzp_key="rzp_test_JH8NWoQAIddV8m", order_id=oid,
                            name=name, email=email, mobile=mobile, amount_rs=amount, callback_url="/donation/request/8/callback")

    # return {"msg": razorpay_response.json()}
    
if __name__=='__main__':
    app.run(host='localhost',port=5000,debug=True)